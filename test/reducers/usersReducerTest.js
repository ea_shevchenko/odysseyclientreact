import expect from 'expect';
import reducers from '../../src/reducers';

describe('reducers', () => {
  it('should handle users fetch action', () => {
    let state;
    state = reducers({
      users:{payload:[]},
      ajax:{loading:false,error:false,errorMessage:''},
      authenticated:{authenticated:true},
      metric:{payload:[4,1]}}, {type:'FETCH_USERS',payload:[{id:1,login:'string', password:'$2a$10$d5I3xVVfVlQoo0LHGvh8lOrtxK05RKBJwHuWJxlYKK47alV9R4t3S'}]});
    expect(state.users.payload.length).toEqual(1);
    expect(state.users.payload[0]).toEqual({password:'$2a$10$d5I3xVVfVlQoo0LHGvh8lOrtxK05RKBJwHuWJxlYKK47alV9R4t3S',login:'string',id:1});
  });

  it('should handle user fetch action', () => {
    let state;
    state = reducers({
      users:{payload:[{id:1,login:'string',password:'$2a$10$d5I3xVVfVlQoo0LHGvh8lOrtxK05RKBJwHuWJxlYKK47alV9R4t3S'}]},
      ajax:{loading:false,error:false,errorMessage:''},
      authenticated:{authenticated:true},
      metric:{payload:[4,1]}}, {type:'FETCH_USER',payload:[{id:1,login:'string',avatar_url:'string',password:'$2a$10$d5I3xVVfVlQoo0LHGvh8lOrtxK05RKBJwHuWJxlYKK47alV9R4t3S',userRoles:[{id:1,listRole:'ADMIN'}]}]});
    expect(state.users.payload[0].userRoles).toEqual([{listRole:'ADMIN',id:1}]);
    expect(state.users.payload[0].avatar_url).toEqual('string');
  });


});
