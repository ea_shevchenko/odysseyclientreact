import expect from 'expect';
import reducers from '../../src/reducers';

describe('reducers', () => {
  it('should handle actions', () => {
    let state;
    state = reducers({users:{payload:[]},ajax:{loading:false,error:false,errorMessage:''},authenticated:{authenticated:true},metric:{payload:[]}}, {type:'FETCH_STATUS_METRIC',payload:[71,1,1,2]});
    expect(state.metric.payload.length).toEqual(4);
    expect(state.metric.payload[0]).toEqual(71);
    expect(state.metric.payload[1]).toEqual(1);
    expect(state.metric.payload[2]).toEqual(1);
    expect(state.metric.payload[3]).toEqual(2);
  });
});
