import {beginAjaxCall, ajaxCallSuccess, ajaxCallError, revertAjaxStatusState} from '../ajax/ajaxDispatchActions';
import {fetchUsersRequest, fetchUserRequest} from './usersDispatchActions';
import {jwtTokenHeader} from '../../constants/jwtConstants';
import {USERS_URL} from '../../constants/urlConstants';
import AjaxObservable from 'rxjs/observable/dom/AjaxObservable';
import 'rxjs';
import {Observable} from 'rxjs/Observable';

export const fetchUsers = () => {
  return (dispatch) => {
    dispatch(revertAjaxStatusState());
    dispatch(beginAjaxCall());
    AjaxObservable.ajaxGet(USERS_URL, jwtTokenHeader())
      .map(data => data.response)
      .delay(1000)
      .catch((err) => {
        dispatch(ajaxCallError(err));
        return Observable.empty();
      })
      .subscribe(data => {
        if (data.length != 0) {
          dispatch(revertAjaxStatusState());
          dispatch(ajaxCallSuccess());
          dispatch(fetchUsersRequest(data));
        }
      });
  };
};

export const fetchUserById = (userId) => {
  return (dispath) => {
    dispath(revertAjaxStatusState());
    dispath(beginAjaxCall());
    AjaxObservable.ajaxGet(`${USERS_URL}/id/${userId}`,jwtTokenHeader())
      .map(data => data.response)
      .delay(1000)
      .catch((err) => {
        dispath(ajaxCallError(err));
        return Observable.empty();
      })
      .subscribe(data => {
        dispath(ajaxCallSuccess());
        dispath(fetchUserRequest(data));
      });
  }
};

export const fetchUserByName = (userName) => {
  return (dispatch) => {
    dispatch(revertAjaxStatusState());
    dispatch(beginAjaxCall());
    AjaxObservable.ajaxGet(`${USERS_URL}/name/${userName}`, jwtTokenHeader())
      .map(data => data.response)
      .delay(1000)
      .catch((err) => {
        dispatch(ajaxCallError(err));
        return Observable.empty();
      })
      .subscribe(data => {
        dispatch(ajaxCallSuccess());
        dispatch(fetchUserRequest(data));
      });
  }
};



