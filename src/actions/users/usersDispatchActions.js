import * as actionTypes from '../actionTypes';

export function fetchUsersRequest(data) {
  return {type: actionTypes.FETCH_USERS, payload: data};
}

export function fetchUserRequest(data){
  let userData = [];
  userData.push(data);
  return {type: actionTypes.FETCH_USER, payload: userData};
}
