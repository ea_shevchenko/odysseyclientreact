import {authUserSuccess, authUserFailure} from '../auth/authDispatchActions';
import {beginAjaxCall, ajaxCallSuccess, ajaxCallError, revertAjaxStatusState} from '../ajax/ajaxDispatchActions';
import {browserHistory} from 'react-router';
import AjaxObservable from 'rxjs/observable/dom/AjaxObservable';
import 'rxjs';
import {Observable} from 'rxjs/Observable';
import {BASE_URL} from '../../constants/urlConstants';

export const signIn  = (userName, userPassword) => {
  let jsonParam =  JSON.stringify({'username': userName, 'password': userPassword});
  return (dispatch) => {
    dispatch(revertAjaxStatusState());
    dispatch(beginAjaxCall());
    AjaxObservable.ajaxPost(`${BASE_URL}/auth`, jsonParam,{'Content-Type': 'application/json'} )
      .map(data => data.response)
      .catch((err) => {
        dispatch(ajaxCallError(err));
        return Observable.empty();
      })
      .subscribe(data => {
        dispatch(ajaxCallSuccess());
        dispatch(authUserSuccess());
        localStorage.setItem('token', data.token);
        browserHistory.push('admin');
      });
  }
};

export const signOut = () => {
  return (dispatch) => {
    dispatch(revertAjaxStatusState());
    dispatch(authUserFailure());
    localStorage.removeItem('token');
    browserHistory.push('');
  }
};
