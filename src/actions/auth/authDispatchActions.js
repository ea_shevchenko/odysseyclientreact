import * as actionTypes from '../actionTypes';

export const authUserSuccess = () => {
  return {type: actionTypes.AUTH_USER_SUCCESS};
};

export const authUserFailure = () => {
  return {type: actionTypes.AUTH_USER_FAILURE};
};


