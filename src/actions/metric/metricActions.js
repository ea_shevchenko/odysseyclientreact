import {ajaxCallError,ajaxCallSuccess, beginAjaxCall, revertAjaxStatusState} from '../ajax/ajaxDispatchActions';
import {fetchStatusMetricResponse} from './metricDispatchActions';
import {METRIC_STATUS_URL} from '../../constants/urlConstants';
import AjaxObservable from 'rxjs/observable/dom/AjaxObservable';
import 'rxjs';
import {Observable} from 'rxjs/Observable';

export const fetchStatusMetric = () => {
  return (dispatch) => {
    dispatch(revertAjaxStatusState());
    dispatch(beginAjaxCall());
    AjaxObservable.ajaxGet(METRIC_STATUS_URL)
      .delay(500)
      .map(data => data.response)
      .map(data => {
        let arrData = Object.keys(data).map(key => data[key]);
        return arrData;
      })
      .catch((err) =>{
       dispatch(ajaxCallError(err));
       return new Observable.empty();
      })
      .subscribe(data => {
        dispatch(ajaxCallSuccess());
        dispatch(fetchStatusMetricResponse(data));
      })
  }
};

export const fetchFullMetric = () => {
  return (dispatch) => {
    dispatch(revertAjaxStatusState());
    dispatch(beginAjaxCall());
  }
};

export const fetchGraphMetric = () => {
  return (dispatch) => {
    dispatch(revertAjaxStatusState());
    dispatch(beginAjaxCall());

  }
};

