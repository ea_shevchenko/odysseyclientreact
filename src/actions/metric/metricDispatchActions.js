import * as actionTypes from '../actionTypes';

export const fetchStatusMetricResponse = (data) => {
  return {type: actionTypes.FETCH_STATUS_METRIC, payload: data};
};


export const fetchFullMetricResponse = (data) => {
  return {type: actionTypes.FETCH_FULL_METRIC, payload: data};
};


export const fetchGraphMetricResponse = (data) => {
  return {type: actionTypes.FETCH_GRAPH_METRIC, payload: data};
};
