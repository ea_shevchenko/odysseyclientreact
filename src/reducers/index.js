import {combineReducers} from 'redux';
import usersReducer from './usersReducer';
import ajaxReducer from './ajaxReducer';
import authReducer from './authReducer';
import metricReducer from './metricsReducer';

const rootReducer = combineReducers({
  users: usersReducer,
  ajax: ajaxReducer,
  authenticated: authReducer,
  metric: metricReducer
});

export default rootReducer;
