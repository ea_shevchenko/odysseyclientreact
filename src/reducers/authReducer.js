import * as actionTypes from '../actions/actionTypes';

const init = {
  authenticated: false
};

export default function  authReducer(state = init, action) {
  switch (action.type) {
    case actionTypes.AUTH_USER_SUCCESS:
      return {...state, authenticated: true};
    case actionTypes.AUTH_USER_FAILURE:
      return {...state, authenticated: false};
    default:
      return state;
  }
}
