import * as actionTypes from '../actions/actionTypes';
const initAjaxState = {
  loading:false,
  error: false,
  errorMessage: ''
};

export default function ajaxReducer(state = initAjaxState, action) {
  switch (action.type){
    case actionTypes.BEGIN_AJAX_CALL:
      return {...state, loading: action.loading};
    case actionTypes.REVERT_AJAX_STATUS_STATE:
      return {...state, loading: action.loading, error: action.error, errorMessage: action.errorMessage};
    case actionTypes.AJAX_CALL_SUCCESS:
      return {...state, loading: action.loading};
    case actionTypes.AJAX_CALL_ERROR:
      return {...state, error: action.error, errorMessage: action.errorMessage};
    case actionTypes.AJAX_CALL_NO_DATA:
      return {...state, error: action.error, noData: action.noData, loading: action.loading}
    default:
      return state;
  }
}
