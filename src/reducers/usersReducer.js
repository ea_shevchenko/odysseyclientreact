import * as types from '../actions/actionTypes';

const initUsersState = {
  payload: []
};

export default function usersReducer(state = initUsersState, action) {
  switch (action.type) {
    case types.FETCH_USERS:
      return {...state, payload: action.payload};
    case types.FETCH_USER:
      return{...state, payload: action.payload};
    default:
      return state;
  }
}
