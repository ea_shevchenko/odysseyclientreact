import * as actionTypes from '../actions/actionTypes';

const initUsersState = {
  payload: []
};

export default function metricReducer(state = initUsersState, action){
  switch (action.type) {
    case actionTypes.FETCH_STATUS_METRIC:
      return {...state, payload: action.payload};
    default:
      return  state;
  }
}
