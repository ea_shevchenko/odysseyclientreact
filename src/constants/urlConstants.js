const HTTP_MODE_URL = 'http://localhost:8082';
const HTTPS_MODE_URL = 'https://localhost:8443';

export const BASE_URL = HTTP_MODE_URL || HTTPS_MODE_URL;

export const API_URL = `${BASE_URL}/api/`;

export const METRIC_URL =  `${BASE_URL}/metric`;
export const METRIC_STATUS_URL = `${METRIC_URL}/status`;
export const METRIC_FULL_URL = `${METRIC_URL}/full`;
export const METRIC_GRAPH_URL = `${METRIC_URL}/graph`;

export const USERS_URL = `${API_URL}/users`;

