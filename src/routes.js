import React from 'react';
import {Route, IndexRoute} from 'react-router';
import App from './components/App';
import AdminPage from './containers/admin/AdminPage';
import UsersPage from './containers/users/UsersPage';
import LoginPage from './containers/users/LoginPage';
import MetricPage from './containers/metric/MetricPage';
import AboutPage from './components/about/AboutPage';

import Auth from './components/auth/Auth';

export default (
    <Route path='/' component={App}>
      <IndexRoute component={LoginPage} />
      <Route path='admin' component={Auth(AdminPage)}>
        <Route path='users' component={Auth(UsersPage)} />
        <Route path='about' component={Auth(AboutPage)} />
        <Route path='metrics' component={Auth(MetricPage)} />
      </Route>
    </Route>
);
