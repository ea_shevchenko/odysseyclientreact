import React from 'react';
import {Link} from 'react-router';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/arrow-drop-down-circle';
import AppBar from 'material-ui/AppBar';

const Header = (props) => {

  return (
    <div>
      <AppBar title="Demo app"
              showMenuIconButton={false}
              iconElementRight={
                <IconMenu
                  iconButtonElement={<IconButton><MoreVertIcon /></IconButton>}
                  targetOrigin={{horizontal: 'right', vertical: 'top'}}
                  anchorOrigin={{horizontal: 'right', vertical: 'top'}}>
                  <MenuItem containerElement={<Link to="/admin"/>} primaryText="home"/>
                  <MenuItem containerElement={<Link to="/admin/about"/>} primaryText="about"/>
                  <MenuItem containerElement={<Link to="/admin/users"/>} primaryText="users"/>
                  <MenuItem containerElement={<Link to="/admin/metrics"/>} primaryText="metrics"/>
                  <MenuItem  onTouchTap={props.signOut} primaryText="sign out"/>
                </IconMenu>}>
      </AppBar>
    </div>
  );
};

export default Header;
