import * as React from 'react';
import {List} from 'material-ui/List';
import Divider from 'material-ui/Divider';

const RightBarHeader = () =>{
  return(
    <div className='rightBarHeader'>
      <List>
      </List>
      <Divider />
    </div>
  );
};

export default RightBarHeader;
