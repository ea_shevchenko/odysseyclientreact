import React from 'react';
import {Chart} from 'react-google-charts';

const GoogleChart = (props) => {
  return(
    <div>
      <Chart chartType={props.type} rows={props.rows} columns={props.columns} options={props.options} graph_id='ScatterChart'  width={'50%'} height={'100px'}  legend_toggle={true} />
    </div>
  );
};
export default GoogleChart;
