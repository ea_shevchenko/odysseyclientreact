export const styles = {
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around'
  },
  gridList: {
    width: '100%',
    height: 450,
    overflowY: 'auto',
    marginBottom: 2
  },
  gridTile: {
    width: '100%',
    height: '100%'
  },
  loginPaper:{
    position: 'fixed',
    top: 0,
    bottom:0,
    left:0,
    right:0,
    margin:'auto',
    height: 400,
    width: 500,
    textAlign: 'center'
  },
  logoAvatar:{
    size: 90
  },
  loginButton:{
    margin: 60
  }
};
