import Header from '../../components/common/Header';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as authActions from '../../actions/auth/authActions';
import React, {PropTypes} from 'react';

class HomePage extends React.Component {

  constructor(props) {
    super(props);
    this.handleSighOut = this.handleSighOut.bind(this);
  }

  handleSighOut(event) {
    event.preventDefault();
    this.props.authActions.signOut();
  }

  render() {
    return (
      <div>
        <Header signOut={this.handleSighOut}/>
        {this.props.children}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    users: state.users,
    ajax: state.ajax
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    authActions: bindActionCreators(authActions, dispatch)
  };
};

HomePage.propTypes = {
  users: PropTypes.object.isRequired,
  ajax: PropTypes.object.isRequired,
  authActions: PropTypes.object.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
