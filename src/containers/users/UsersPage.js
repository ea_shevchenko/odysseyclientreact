import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as userActions from '../../actions/users/userActions';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import CircularProgress from 'material-ui/CircularProgress';
import Snackbar from 'material-ui/Snackbar';
import Avatar from 'material-ui/Avatar';
import TextField from 'material-ui/TextField';
import UserAvatar from 'material-ui/svg-icons/social/person-add';

class UsersPage extends React.Component {

  constructor(props) {
    super(props);
    this.getUsers = this.getUsers.bind(this);
    this.getUserById = this.getUserById.bind(this);
    this.getUserByName = this.getUserByName.bind(this);
    this.handleChangeUserId = this.handleChangeUserId.bind(this);
    this.handleChangeUserName = this.handleChangeUserName.bind(this);
  }

  getUsers(event) {
    event.preventDefault();
    this.props.userActions.fetchUsers();
  }

  getUserById(event) {
    event.preventDefault();
    this.props.userActions.fetchUserById(this.state.id);
  }

  getUserByName(event) {
    event.preventDefault();
    this.props.userActions.fetchUserByName(this.state.name);
  }

  handleChangeUserId(event) {
    event.preventDefault();
    this.setState({id: event.target.value});
  }

  handleChangeUserName(event) {
    event.preventDefault();
    this.setState({name: event.target.value});
  }

  getUserRoles(userRoles) {
    let rolesArr = [];
    if (userRoles) {
      userRoles.map((role) => {
        rolesArr.push(role.listRole);
      });
    }
    return rolesArr.join(',');
  }

  getAvatarSource(dataSource) {
    let isImage = true;
    if (dataSource) {
      isImage = dataSource.indexOf('.png');
    }
    return isImage ? <Avatar icon={<UserAvatar/>}/> : <Avatar src={dataSource}/>;
  }

  tableDataContent() {
    let userData = this.props.users.payload;
    let tableRows = userData.map((dataItem, i) => {
      return <TableRow key={i}>
        <TableRowColumn>{dataItem.id}</TableRowColumn>
        <TableRowColumn>{dataItem.login}</TableRowColumn>
        <TableRowColumn>{this.getAvatarSource(dataItem.avatar_url)}</TableRowColumn>
        <TableRowColumn>{this.getUserRoles(dataItem.userRoles)}</TableRowColumn>
      </TableRow>;
    });
    return <Table>
      <TableHeader>
        <TableRow>
          <TableHeaderColumn>ID</TableHeaderColumn>
          <TableHeaderColumn>Name</TableHeaderColumn>
          <TableHeaderColumn>Avatar</TableHeaderColumn>
          <TableHeaderColumn>Role</TableHeaderColumn>
        </TableRow>
      </TableHeader>
      <TableBody>
        {tableRows}
      </TableBody>
    </Table>;
  }

  render() {
    return (
      <div className='row'>
        <RaisedButton label="get users" backgroundColor="green" onTouchTap={this.getUsers}/>
        <RaisedButton label="get user by id" backgroundColor="yellow" onTouchTap={this.getUserById}/>
        <RaisedButton label="get user by name" backgroundColor="brown" onTouchTap={this.getUserByName}/>
        <br/>
        <TextField hintText="user id" onChange={this.handleChangeUserId}/>
        <br/>
        <TextField hintText="user name" onChange={this.handleChangeUserName}/>
        <div>
          {this.props.ajax.error ? <Snackbar open={this.props.ajax.error}
                                             message={this.props.ajax.noData ? 'No data' : this.props.ajax.errorMessage}
                                             autoHideDuration={4000}/>
            : (this.props.ajax.loading ? <CircularProgress size={1.0}/> : this.tableDataContent())}
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    users: state.users,
    ajax: state.ajax
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userActions: bindActionCreators(userActions, dispatch)
  };
};

UsersPage.propTypes = {
  users: PropTypes.object.isRequired,
  ajax: PropTypes.object.isRequired,
  userActions: PropTypes.object.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersPage);
