import React, {PropTypes} from 'react';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import Avatar from 'material-ui/Avatar';
import RaisedButton from 'material-ui/RaisedButton';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as authActions from '../../actions/auth/authActions';
import * as style from '../../styles/AppStyle';
let image = require('../../images/material-design-logo.png');

class LoginPage extends React.Component{

  constructor(props) {
    super(props);
    this.auth = this.auth.bind(this);
  }

  auth(event){
    event.preventDefault();
    this.props.authActions.signIn(this.refs.username.getValue(), this.refs.userpassword.getValue());
  }

  render(){
    return(
        <Paper style={style.styles.loginPaper} zDepth={1}>
          <br/>
          <Avatar src={image} size={style.styles.logoAvatar.size}></Avatar>
          <br/>
          <TextField floatingLabelText="user name"  type="" ref="username"/>
          <br/>
          <TextField floatingLabelText="user password" ref="userpassword" type="password"/>
          <br/>
          <RaisedButton label="Login" secondary={true} style={style.styles.loginButton} onTouchTap={this.auth}></RaisedButton>
        </Paper>);
  }
}

function mapStateToProps(state) {
  return {
    authenticated: state.authenticated
  };
}

function mapDispatchToProps(dispatch) {
  return {
    authActions: bindActionCreators(authActions, dispatch)
  };
}

LoginPage.propTypes = {
  authenticated: PropTypes.object.isRequired,
  authActions: PropTypes.object.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
