import React, {PropTypes} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as metricActions from '../../actions/metric/metricActions'
import GoogleChart from '../../components/charts/GoogleChart'
import RaisedButton from 'material-ui/RaisedButton';

class MetricPage extends React.Component{

  constructor(props) {
    super(props);
    this.handleStatusMetric = this.handleStatusMetric.bind(this);
  }

  handleStatusMetric(event){
    event.preventDefault();
    this.props.metricActions.fetchStatusMetric();
  }
  chartOpt = {
  options: {
    hAxis: {title: 'code', minValue: 0, maxValue: 15},
    vAxis: {title: 'count', minValue: 0, maxValue: 3000},
    legend: 'none'
  },
  rows: [
    [ 8,      12],
    [ 4,      5.5],
    [ 11,     14],
    [ 4,      5],
    [ 3,      3.5],
    [ 6.5,    7]
  ],
  columns: [
    {
      'type': 'number',
      'label' : 'code'
    },
    {
      'type' : 'number',
      'label' : 'count'
    }
  ]
};


  render(){
    return(
      <div>
        <RaisedButton label="get metrics" backgroundColor="white" onTouchTap={this.handleStatusMetric}/>
        <br/>
          <GoogleChart type="ScatterChart" rows={this.chartOpt.rows} columns={this.chartOpt.columns} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    metric: state.metric,
    ajax: state.ajax
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    metricActions: bindActionCreators(metricActions, dispatch)
  };
};

MetricPage.propTypes  = {
  metric: PropTypes.object.isRequired,
  ajax: PropTypes.object.isRequired,
  metricActions: PropTypes.object.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(MetricPage);
